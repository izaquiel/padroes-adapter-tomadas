
public class App {
	
	public static void main(String[] args) {
		PlugueEuropeu plugueEuropeu = new PlugueEuropeu();
		AdaptadorEuropeuComum adapEuroComum = new AdaptadorEuropeuComum();
		
		PlugueComum plugueComum = new PlugueComum();
		AdaptadorComumEuropeu adapComumEuropeu = new AdaptadorComumEuropeu();
		
		plugueEuropeu.conectar(adapEuroComum);
		plugueComum.conectar(adapComumEuropeu);
		
	}
}
